# HPC Project

Pour traiter des données de DNase dans le but de répondre à la question biologique, j’ai implémenté un pipeline de traitement en utilisant SLURM (Simple Linux Utility for Resource Management) qui est un gestionnaire de ressources très utilisé dans les environnements de calcul à haute performance (HPC). Ainsi, le pipeline qui a été construite à l’aide de différents jobs qui dépendent les uns des autres ce qui forme une séquence d’exécution. Ainsi, les dépendances entre les jobs ont été formatées en utilisant le paramètre « afterok ».

## Installation


```bash
pip install subprocess
conda install -c bioconda homer
conda install -c bioconda bedops
if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install(version = "3.18")
BiocManager::install("NOIseq")
```
## Architecture

```
working directory = /home/users/student08
mkdir m2bi_project in the working directory
put all scripts on /home/users/student08/m2bi_project/scripts
run command from the working directory

Genome de réference => mm39



m2bi_project
|-- data
|-- Reference_Genome
|-- results
|   |-- dnase-seq
|   |   |-- mapping
|   |   |-- matrix
|   |   |-- multiqc_raw_data
|   |   |-- multiqc_trimmed_data
|   |   |-- peaks_calling
|   |   |-- qc_raw_data
|   |   |-- qc_trimmed_data
|   |   `-- trimming
|   |       `-- log
|   `-- Statistics
`-- scripts

```



## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Author
<p>DIOUF Seydina Mouhamed</p>
<p>Master's degree in Bioinformatics at Clermont Auvergne University</p>

## Contact  
- email: seydinamouhameddiouf808@gmail.com  
- LinkedIn : https://linkedin.com/in/seydina-mouhamed-diouf-50348a1ab
- Gitlab : https://gitlab.com/Seydina99

## Contributor
<p>Nadia GOUE</p>

<table align="center"><tr><td align="center" width="9999">
<img src="Images/logo_uca.png" align="center"  />
</table>
