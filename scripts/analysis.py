import subprocess


t = []
list_CPU_E = []
list_MEM_U = []
list_MEM_E = []

with open("/home/users/student08/m2bi_project/results/Statistics/Seff.txt", "r") as f1:
	for li in f1:
		li = li.rstrip()
		# Execution time
		if li.startswith("CPU Utilized"):
			execution_time = li.split("Utilized:")[1]
			t.append(execution_time)

		# CPU Efficiency
		if li.startswith("CPU Efficiency"):
			CPU_E =	li.split("Efficiency:")[1].split("core")[0]
			list_CPU_E.append(CPU_E)
	
		# Memory Utilized
		if li.startswith("Memory Utilized"):
			MEM_U = li.split("Utilized:")[1]
			list_MEM_U.append(MEM_U)

		# Memory Efficiency
		if li.startswith("Memory Efficiency"):
			MEM_E = li.split("Efficiency:")[1]
			list_MEM_E.append(MEM_E)

index = ["QC raw data", "Multiqc raw data", "Trimming", "QC trimmed data", "Multiqc trimmed data", "Mapping", "Peaks Calling", "Matrix conversion", "Statistics" ]


#subprocess.run("touch performances.txt", shell=True)

f = open("/home/users/student08/m2bi_project/results/Statistics/performances.txt", "a")
break_line = "\n"
f.write("Parameters ; Execution time ; CPU Efficiency ; Memory Utilized ; Memory Efficiency")
f.write(break_line)

for i in range(len(index)):
	li = str(index[i] + ";" + t[i] + ";" + list_CPU_E[i] + ";" + list_MEM_U[i] + ";" + list_MEM_E[i]) 
	f.write(li)
	f.write(break_line)
f.close()
