#!/bin/bash
#SBATCH -o log/slurmjob-%A-%a

echo 'Date: Fall Master course 2023'
echo 'Object: Sample workflow for Stategra datasets showing job execution and dependency handling.'
echo 'Inputs: paths to scripts for quality control and triming'
echo 'Outputs: trimmed fastq files and QC HTML files'

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose


# Initial 
jid0=$(sbatch --parsable /home/users/student08/m2bi_project/scripts/default_script.slurm)
echo "$jid0 : Initial"

# Initial QC
jid1=$(sbatch --parsable  --dependency=afterok:$jid0 /home/users/student08/m2bi_project/scripts/stategra_qc-init.slurm)
echo "$jid1 : Initial Quality Control"
#sacct -j $jid1 >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt
#echo " " >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt

# Initial MultiQC
jid2=$(sbatch --parsable --dependency=afterok:$jid1 /home/users/student08/m2bi_project/scripts/stategra_multiqc_raw_data.slurm)
echo "$jid2 : Initial MultiQC"
#sacct -j $jid2 >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt
#echo " " >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt

# Trimming
jid3=$(sbatch --parsable --dependency=afterok:$jid1 /home/users/student08/m2bi_project/scripts/stategra_trim.slurm)
echo "$jid3 : Trimming with Trimmomatic tool"
#sacct -j $jid3 >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt
#echo " " >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt

# Post QC on trimmed data
jid4=$(sbatch --parsable --dependency=afterok:$jid3 /home/users/student08/m2bi_project/scripts/stategra_qc-post.slurm)
echo "$jid4 : Post control_quality"
#sacct -j $jid4 >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt
#echo " " >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt

# MultiQC on trimmed data
jid5=$(sbatch --parsable --dependency=afterok:$jid4 /home/users/student08/m2bi_project/scripts/stategra_multiqc_trimmed_data.slurm)
echo "$jid5 : MultiQC on trimmed data"
#sacct -j $jid5 >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt
#echo " " >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt

# Mapping
jid6=$(sbatch --parsable --dependency=afterok:$jid4 /home/users/student08/m2bi_project/scripts/stategra_mapping.slurm)
echo "$jid6 : Mapping"
#sacct -j $jid6 >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt
#echo " " >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt

# Peak Calling
jid7=$(sbatch --parsable --dependency=afterok:$jid6 /home/users/student08/m2bi_project/scripts/stategra_peaks_calling.slurm)
echo "$jid7 : Peak Calling"
#sacct -j $jid7 >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt
#echo " " >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt

# Matrix conversion
jid8=$(sbatch --parsable --dependency=afterok:$jid7 /home/users/student08/m2bi_project/scripts/stategra_matrix_conversion.slum)
echo "$jid8 : Matrix conversion"
#sacct -j $jid8 >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt
#echo " " >> /home/users/student08/m2bi_project/results/Statistics/Stats.txt

# Statistics
jid9=$(sbatch --parsable  --dependency=afterok:$jid8 /home/users/student08/m2bi_project/scripts/statistics.slurm)
echo "$jid9 : Statistic analysis"

list_jobs="$jid1 $jid2 $jid3 $jid4 $jid5 $jid6 $jid7 $jid8 $jid9"
echo "Jobs list : ${list_jobs}"

STATS_DIR=/home/users/student08/m2bi_project/results/Statistics
mkdir ${STATS_DIR}
cd ${STATS_DIR}
touch list_jobs.txt
cd
echo $list_jobs >> ${STATS_DIR}/list_jobs.txt
